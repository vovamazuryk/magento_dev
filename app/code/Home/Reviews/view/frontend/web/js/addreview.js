require(
    [
        'jquery',
        'mage/validation'
    ],

    function( $, mage) {

        $("#formsend").click(function(event){

            var dataForm = $('#review-form');
            var isvalid = dataForm.validation('isValid');

            if (isvalid) {
                event.preventDefault();
                var param = $('#review-form').serialize();
                $.ajax({
                    showLoader: true,
                    url: '/reviews/index/addreview',
                    data: param,
                    type: "POST"
                }).done(function (msg) {
                    $('.message-success').html('<div data-bind="html: message.text">' + msg + '</div>');
                    $('.messages').show('slow');
                });
            }
        });

    }

);