<?php
namespace Home\Reviews\Model;

use Home\Reviews\Model\ResourceModel\Reviews\CollectionFactory;


class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    public function __construct($name,$primaryFieldName,$requestFieldName,
    CollectionFactory $employeeCollectionFactory,
    \Magento\Framework\App\RequestInterface $request,
    array $meta = [],
    array $data = []
    )
    {
        $this->collection = $employeeCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->_request = $request;
    }

    public function getData()
    {
        $itemId = $this->_request->getParam('id');
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        $this->loadedData = array();
        foreach ($items as $item) {
            $this->loadedData[$itemId] = $item->getData();
        } //echo '<pre>';print_r($this->loadedData);echo '</pre>';exit;

        return $this->loadedData;

    }
}