<?php

namespace Home\Reviews\Model;

use \Magento\Framework\Model\AbstractModel;

class Reviews extends AbstractModel
{


    /**
     * Initialize resource model
     * @return void
     */
    public function _construct()
    {
        $this->_init('Home\Reviews\Model\ResourceModel\Reviews');
    }

}

