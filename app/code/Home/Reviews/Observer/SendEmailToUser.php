<?php


namespace Home\Reviews\Observer;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Mail\Template\TransportBuilder;

class SendEmailToUser implements \Magento\Framework\Event\ObserverInterface
{
    protected $_scopeConfig;
    protected $_transportBuilder;

    public function __construct(
        TransportBuilder $transportBuilder,
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->_transportBuilder=$transportBuilder;
        $this->_scopeConfig = $scopeConfig;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

            $adminEmail = $this->_scopeConfig->getValue('trans_email/ident_support/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $userEmail = $observer->getData('user_email');
            $transport = $this->_transportBuilder
                ->setTemplateIdentifier('send_user_email_review_enable')
                ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID])
                ->setTemplateVars(['enable_rew' => 'Yor review was enable',])
                ->setFrom(['name' => 'Admin from site magento', 'email' => $adminEmail])
                ->addTo($userEmail)
                ->getTransport();
            $transport->sendMessage();

    }

}