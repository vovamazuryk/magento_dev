<?php
/**
 * Created by Volodymyr Mazuryk.
 * User: vovan
 * Date: 07.11.18
 * Time: 12:43
 */

namespace Home\Reviews\Block;

use Magento\Theme\Block\Html\Pager;

class Rewpager extends Pager
{

    protected $_template = 'Home_Reviews::pager.phtml';

    protected $_availableLimit = [3 => 3, 6 => 6, 10 => 10];

    protected $_displayPages = 3;

    protected function _construct()
    {
        parent::_construct();
        $this->setData('show_amounts', true);
        $this->setData('use_container', true);
    }

}