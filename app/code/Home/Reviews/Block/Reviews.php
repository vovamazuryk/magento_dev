<?php
namespace Home\Reviews\Block;

class Reviews extends \Magento\Framework\View\Element\Template
{
    protected $_reviewsFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Home\Reviews\Model\ReviewsFactory $reviewsFactory
    )
    {
        $this->_reviewsFactory = $reviewsFactory;
        parent::__construct($context);
    }

    public function sayReviews()
    {
        return __('Testimonials');
    }

    public function addReview()
    {

        return $this->getUrl('reviews/index/addreview');
    }

    public function getReviewsCollection()
    {
        $page=($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
        $pageSize=($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 3;

        $review = $this->_reviewsFactory->create()->getCollection()->addFieldToFilter('is_active', 1);
        $review->setPageSize($pageSize)->setCurPage($page);

        return $review;
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        if ($this->getReviewsCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Home\Reviews\Block\Rewpager',
                'rew.reviews.pager'
            )->setShowPerPage(true)->setCollection(
                $this->getReviewsCollection()
            );
            $this->setChild('pager', $pager);
            $this->getReviewsCollection()->load();
        }
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

}