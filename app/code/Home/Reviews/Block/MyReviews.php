<?php
/**
 * Created by Volodymyr Mazuryk.
 * User: vovan
 * Date: 19.09.19
 * Time: 9:52
 */

namespace Home\Reviews\Block;

class MyReviews extends \Magento\Framework\View\Element\Template
{
    protected $_reviewsFactory;
    protected $_currentCustomer;


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Home\Reviews\Model\ReviewsFactory $reviewsFactory,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer
    ) {
        $this->_reviewsFactory = $reviewsFactory;
        $this->_currentCustomer = $currentCustomer;
        parent::__construct($context);
    }

    public function getRevCollection() {

        if (!($customerId = $this->_currentCustomer->getCustomerId())) {
            return false;
        }
        $collection = $this->_reviewsFactory->create()->getCollection();
        $collection->addFieldToFilter('is_active', 1)
                   ->addCustomerFilter($customerId);

        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 5;

        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);


        return $collection;
    }

    public function getReviewLink($idd)
    {
        return $this->getUrl('reviews/customer/delete',['id' => $idd]);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
       // $this->pageConfig->getTitle()->set(__('Custom Pagination'));
        if ($this->getRevCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'custom.history.pager'
            )->setAvailableLimit([5 => 5, 10 => 10, 15 => 15, 20 => 20])
                ->setShowPerPage(true)->setCollection($this->getRevCollection())
            ;
            $this->setChild('pager', $pager);
        }
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}