<?php
namespace Home\Reviews\Block;

class Addreview extends \Magento\Framework\View\Element\Template
{
    protected $httpContext;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\App\Http\Context $httpContext,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->httpContext = $httpContext;
    }

    public function custLoggedIn()
    {
        return $this->httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
    }

    public function getAction()
    {
        return '/reviews/index/addreview';
    }
}