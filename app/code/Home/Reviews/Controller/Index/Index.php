<?php

namespace Home\Reviews\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Index extends Action
{
    protected $_pageFactory;
    protected $_scopeConfig;
    protected $_cacheList;
    protected $_cachePool;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        ScopeConfigInterface $scopeConfig)
    {
        $this->_pageFactory = $pageFactory;
        $this->_scopeConfig = $scopeConfig;
        return parent::__construct($context);
    }


    public function execute()
    {

        $_title = $this->_scopeConfig->getValue('reviews/general/display_title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $_title = $_title?$_title:'Your Title';
        $_keywords = $this->_scopeConfig->getValue('reviews/general/display_keywords', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $_keywords = $_keywords?$_keywords:'Your Keywords';
        $_description = $this->_scopeConfig->getValue('reviews/general/display_description', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $_description = $_description?$_description:'Your Description';

        $resultPage = $this->_pageFactory->create();
        $resultPage->getConfig()->getTitle()->set($_title);
        $resultPage->getConfig()->setDescription($_description);
        $resultPage->getConfig()->setKeywords($_keywords);

        return $resultPage;
    }
}

