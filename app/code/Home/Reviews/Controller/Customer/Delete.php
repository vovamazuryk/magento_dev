<?php


namespace Home\Reviews\Controller\Customer;

use Magento\Framework\App\Action\Action;

class Delete extends Action
{
    protected $_reviewsFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Home\Reviews\Model\ReviewsFactory $reviewsFactory
    ) {
        $this->_reviewsFactory = $reviewsFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        try {
            $coll = $this->_reviewsFactory->create();
            $coll->load($id);
            $coll->delete();
            $this->messageManager->addSuccess(
                __('Delete successfully !')
            );
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }
}