<?php
/**
 * Created by Volodymyr Mazuryk.
 * User: vovan
 * Date: 03.11.18
 * Time: 11:16
 */

namespace Home\Reviews\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;

class Edit extends Action
{
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        return $resultPage;
    }
}
