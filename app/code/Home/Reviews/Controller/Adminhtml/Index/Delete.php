<?php
/**
 * Created by Volodymyr Mazuryk.
 * User: vovan
 * Date: 05.11.18
 * Time: 14:54
 */

namespace Home\Reviews\Controller\Adminhtml\Index;


use Magento\Framework\App\Action\Action;

class Delete extends Action
{
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        try {
            $banner = $this->_objectManager->get('Home\Reviews\Model\Reviews')->load($id);
            $banner->delete();
            $this->messageManager->addSuccess(
                __('Delete successfully !')
            );
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }
}