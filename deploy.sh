#!/bin/bash

php bin/magento setup:upgrade
php bin/magento setup:di:compile
rm -R pub/static/*
rm -R var/cache/*
rm -R var/view_preprocessed/*
#php bin/magento setup:static-content:deploy -f -a frontend --theme frontend/JNH/lifestyle
php bin/magento setup:static-content:deploy -f
php bin/magento indexer:reindex
php bin/magento cache:flush